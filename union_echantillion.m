function union_echantillion(liste,name_cell,nbr_couche)
name_file = char(name_cell);
%str = char(strcat('Bibliothèque/Dnn/', liste(1),".mat"));
%data = load(str);
%x = size(data.data_masq,1);
%y = size(data.data_masq,2);
ret = zeros(0,0,nbr_couche);
str = char(strcat('Bibliothèque/Union/', name_file));

if (exist(str, "dir") == 0)
    mkdir (str)
end
for i = 1:nbr_couche
    for j = 1:numel(liste)
        str = char(strcat('Bibliothèque/Dnn/', liste(j),".mat"));
        data = load(str);
        ret = union_masque(ret,data.data_masq,nbr_couche);
        contourf(ret(:,:,i))
        colormap('gray')
        img = getframe(gcf);
        imwrite(img.cdata, strcat('Bibliothèque/Union/', name_file , '/' , name_file , '_masque' , int2str(i) , '.png'))
    end
end
end


%FAIT    DATA1 U DATA2 
function ret = union_masque(data1,data2,nbr_couche)
x1 = size(data1,1);
x2 = size(data2,1);
y1 = size(data1,2);
y2 = size(data2,2);


if (x1 > x2)
    xm = x1;
else
    xm = x2;
end
if (y1 > y2)
    ym = y1;
else
    ym = y2;
end

ret = zeros(xm,ym,nbr_couche);

for i = 1:nbr_couche
    for j = 1:xm
        for k = 1:ym
            if (j <= x1 && k <= y1 && data1(j,k,i) == 1)
                ret(j,k,i) = 1;
            end
            if (j <= x2 && k <= y2 && data2(j,k,i) == 1)
                ret(j,k,i) = 1;
            end
        end
    end
end
end
function Mask
%Masque('composant_isolé',10,3)
Masque('melange/Cs_cu1_SH_1-100_corrected.xlsx', 10,2)
end




%OPTION 
%   1 : RAJOUT D'UN COMPOSANT ISOLE
%   2 : RECUPERATION DES MASQUES ET DONNEE D'UN MELANGE
%   3 : ENVOI D'UN DOSSIER QUI VA FAIRE LE MASQUE DE TOUT LES DONNEES EN
%   .XLSX 



function Masque(name_file,nbr_msk,option)
%COORDONNEE MATRICE 591 60



% PARAMETRE DE TEST
%nbr_msk = 10;
%name_file = 'Test2';
%option = 1;
%name_file = input("NameFile : ","s");



%ON MET LE PROGRAMME AU FORMAT LONG POUR EVITER QUE LES VALEURS SOIT
%ARRONDIE
format long;



if option ~= 3
%ON CREE LE FICHIER DE STOCKAGE DES IMAGES EN VERIFIANT QUIL NEXISTE PAS
%DEJA
        
        data = readmatrix(name_file);
        %disp (name_file)
        %disp (data)
        name_file = strrep(name_file,'.xlsx','');
        %disp (name_file)
        match = wildcardPattern + "/";
        name_file = erase(name_file,match);
        %stack = split(name_file);
        %name_file = strcat(stack(size(stack,1)+1),'');
        %disp(name_file)
        if exist(strcat(strcat('Image_sauvegarde/',name_file),'_result'), 'dir') == 0
            mkdir(strcat(strcat('Image_sauvegarde/',name_file),'_result'))
        end

%ON LIT LA MATRICE DU FICHIER .XLSX

%ON SUPPPRIME LES LIGNES INUTILES
        data(:,1) = [];
        data(1,:) = [];
        data(:,1) = [];



% ON INVERE LES DEUX AXES
        new_data = zeros(60, 591);
        i = 1;
        while i <= 60
            j = 1;
            while j <= 591
                new_data(i,j) = data(j,i);
                j = j + 1;
            end
            i = i + 1;
        end
        data = new_data;


%AFFICHAGE DE LA MATRICE SANS FILTRE
        contourf(data);


%SAUVEGARDER L IMAGE D ORIGINE
%img = getframe(gcf);
%imwrite(img.cdata,[name_file,'_result/','original.png'])
%set(gca,'XColor', [250 250 250], 'YColor',[250 250 250])
%[C,H,F] = contourf(data)
%set(gca,'XDir','reverse')



% COULEUR + AXE
        set(gca, 'xlim',[1 591], 'xtick', 0:20:600)
        set(gca, 'ylim',[1 60], 'ytick', 0:5:60);
        colormap('gray');

% maxGL : PLUS GRANDE VALEUR DE LA MATRICE DATA
        maxGL = max(data(:));
% CREATION DE nbr_msk MASQUES DIFFERENTS
        i = 1;
        data_msq = zeros(60, 591,10);
        %disp (ndims(data_msq(1)))
        while i <= nbr_msk
            mask = data >= maxGL/(nbr_msk+5) * i;
            contourf(mask);
            if option == 1
                str = strcat(strcat('Sauvegarde/',name_file),strcat('_masque','.mat'));
            end
            if option == 2
                str = strcat(strcat('Resultat/',name_file),strcat('_masque','.mat'));
            end
                data_msq(:,:,i) = mask;
                img = getframe(gcf);
                %SAUVEGARDE LES IMAGES DES MASQUES
                imwrite(img.cdata,['Image_sauvegarde/',name_file,'_result/','mask',int2str(i),'.png'])

                %analyse_data(data_msq)
                %SAUVEGARDE LES DONNEES
            i = i +1;
        end     
        dnn = analyse_data(data,data_msq);
        save(str,'data_msq')
        %save(str,"dnn")

else
    %SI C'EST UN REPERTOIRE ALORS IL VA OUVRIR LE REPERTOIR ET SELECTIONNER
    %TOUT LES FICHIERS .XLSX
    rep = name_file;
    ext = '*.xlsx';
    chemin = fullfile(rep,ext);
    list = dir(chemin);

    %UTILISE LA FONCTION MASQUE POUR TOUT LES FICHIERS DANS LE REPERTOIRE 
    %AVEC L'OPTION 1  
    for n = 1:numel(list)
        str = strcat(strcat(rep,'/'),list(n).name);
        disp (str);
        Masque(str,nbr_msk,1);
    end
end

% TEST RECUPERATION DONNEE
%example = matfile('Test2mask_1.mat');
%disp (example.mask);
%TEST DE FERET
%disp (mask5);
%out = bwferet(mask)

end



%ANALYSE LES MASQUES
function ret = analyse_data(data,data_msq)
%COMPTE LE NOMBRE DE BIT = 1 DANS LES MASQUES
srfc = analyse_surface(data_msq);
%RECUPERE LA SOMME DES MASQUES
smm = analyse_somme(data,data_msq);
%RECUPERE XMIN YMIN XMAX YMAX DE CHAQUE MASQUE
pstn = analyse_position(data_msq);
%CREE UNE STRUCTURE POUR FACILITER LA SAUVEGARE
ret = struct(data,data_msq,srfc,pstn,smm);
end




%PARCOURS LA MATRICE ET INCREMENTE A CHAQUE BIT = 1
function ret = analyse_surface(data)
ret = zeros(10);
for i = 1:10
    for j = 1:60
        for k = 1:591
            if (data(i,j,k) == 1)
                ret(i) = ret(i) + 1;
            end
        end
    end
end
end




%PARCOURS LA MATRICE ET RECUPERE L ENCADREMENT
function ret = analyse_position(data)
ret = zeros(10,4);
for i = 1:10
    for j = 1:60
        for k = 1:591
            if (data(i,j,k) == 1 && ret(i,2) == 0)
                ret(i,2) = k;
            end
            if (data(i,j,k) == 1 && (ret(i,1) > j || ret(i,1) == 0))
                ret(i,1) = j;
            end
            if (data(i,j,k) == 1 && (ret(i,1) < j || ret(i,1) == 0))
                ret(i,3) = j;
            end
            if (data(i,j,k) == 1 && (ret(i,1) > j || ret(i,1) == 0))
                ret(i,4) = k;
            end
        end
    end
end
end


function ret = analyse_perimetre(data_masq)
total = bwperim(data_masq);
ret = analyse_surface(total);
end


function ret = analyse_somme(data,data_masq)
ret = zeros(10);
for i = 1:10
    for j = 1:60
        for k = 1:591
            if (data_masq(i,j,k) == 1)
                ret(i) = ret(i) + data(i,j);
            end
        end
    end
end
end

function ret = analyse_feret(data_msq)
for i = 1:10
    min = bwferet(data_msq,'MinFeretProperties');
    max = bwferet(data_msq,'MaxFeretProperties');
end

ret = 0;
end

%function test(data_msq)
%bw = bwareafilt(data_msq,2);
%data_msq = imfill(data_msq,'holes');
%[out,LM] = bwferet(bw,'MinFeretProperties');

%end


%function ret = split(str)
%max = size(str);
%disp (max)
%index = max;
%while str(index) ~= '/'
%    index = index - 1;
%end
%ret = zeros(max - index);
%j = 1;
%while (index + j <= max)
%    ret(j) = str(index+j);
%    j = j + 1;
%end

%disp (ret)
%end
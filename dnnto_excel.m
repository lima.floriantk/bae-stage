function dnnto_excel(file_name,data,rep_echtll)
%name_file = strrep(file_name,'.mat','.xlsx');
%copyfile("Feuille_vierge.xlsx",name_file);
if (isfile("Feuille_vierge.xlsx") == 0)
    xlswrite("Feuille_vierge.xlsx",[],'Données');
    xlswrite("Feuille_vierge.xlsx",[],'Information');
end
[~, ~, raw] = xlsread('Feuille_vierge.xlsx','Données');
[~, ~, raw2] = xlsread('Feuille_vierge.xlsx','Information');
s = load(file_name);
%raw() = s.data;
%raw() = s.data_masq;
for i = 1:(s.nbr_couche)
    raw(2+i,1) = num2cell(i);

    raw(2+i,2) = num2cell(strcat(int2str((s.nbr_couche + 1 - i)/s.nbr_couche * 100),"%"));

    raw(2+i,3) = num2cell(s.srfc(i));

    raw(2+i,4) = num2cell(s.pstn(i,1));

    raw(2+i,5) = num2cell(s.pstn(i,2));

    raw(2+i,6) = num2cell(s.pstn(i,3));

    raw(2+i,7) = num2cell(s.pstn(i,4));
    
    raw(2+i,8) = num2cell(s.smm(i));
    
    raw(2+i,9) = num2cell(s.prmtr(i));
    
    raw(2+i,10) = num2cell(s.nbr_obj(i));
    
    raw(2+i,11) = num2cell(s.moy(i));

    raw(2+i,12) = num2cell(s.idc_crcl(i));

    raw(2+i,13) = num2cell(s.ecrt_tp(i));

    raw(2+i,14) = num2cell(s.cg(i,1));

    raw(2+i,15) = num2cell(s.cg(i,2));

    raw(2+i,16) = num2cell(s.cgm(i,1));

    raw(2+i,17) = num2cell(s.cgm(i,2));
end
%raw() = s.nbr_couche;
name_file = strcat(rep_echtll,'.xlsx');
xlswrite(name_file ,raw, 'Données')

s = load(file_name);
%raw() = s.data;
%raw() = s.data_masq;
%disp(data(1,:))
raw2(2,3) = data(1);

raw2(3,3) = data(2);

raw2(4,3) = data(3);

raw2(5,3) = data(4);

raw2(6,3) = data(5);
    
raw2(7,3) = data(6);

raw2(8,3) = data(7);
    
raw2(9,3) = data(8);
    
raw2(10,3) = data(9);

raw2(11,3) = data(10);

raw2(12,3) = data(11);

raw2(13,3) = data(12);

raw2(14,3) = data(13);

raw2(14,4) = data(14);
%raw() = s.nbr_couche;
raw2(15,3) = data(15);
xlswrite(name_file ,raw2, 'Information')
end
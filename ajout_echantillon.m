function ajout_echantillon(name_file,nbr_couche,new_tab)
format longg;

%disp(name_file);
new_name = char(new_tab(1));


data = readmatrix(name_file);

if (contains(name_file, ".txt"))
    data(isnan(data)) = 0;
end

%disp(data)
%disp(data)
%name_file = strrep(name_file,'\','/');
%name_file = strrep(name_file,'.xlsx','');
match = wildcardPattern + "/";
%name_file = erase(name_file,match);
new_name = strrep(new_name,'.xlsx','');
new_name = strrep(new_name,'\','/');
new_name = erase(new_name,match);
chemin = strcat('Bibliothèque','/',char(new_tab(16)),'/');
rep_echtlln = strcat(chemin, new_name , '_result');


if exist(rep_echtlln,"dir") == 0
    mkdir(rep_echtlln)
end

%NOTE SUPPRIMER LES LIGNES ET COLONNES INUTILES

while (iscellstr(data(1,1)))
    data(1,:) = [];
end
data(1,:) = [];
data(:,1) = [];
%data(1,:) = [];
%data(:,1) = [];

new_data = zeros(size(data,2), size(data,1),"double");
i = 1;


while i <= size(data,2)
    j = 1;
    while j <= (size(data,1))
        if (data(j,i) >= 0)
            new_data(i,j) = data(j,i);
        else
            new_data(i,j) = 0;
        end
        j = j+ 1;
    end
    i = i + 1;
end
data = new_data;
disp(data)
%data = data * 1000000;
%disp(data)
%disp(data);
%set(gca,'xlim',[0 size(data,2)], 'xtick', 0:20:size(data,2))
%set(gca, 'ylim',[1 size(data,1)], 'ytick',0:5:size(data,1))
colormap('gray')
contourf(data)
img = getframe(gcf);
imwrite(img.cdata,strcat(rep_echtlln,'/', new_name,'_original.png'))

maxGL = max(data(:));

i = 1;
data_msq = zeros(size(data,1),size(data,2),nbr_couche);

while i <= nbr_couche
    masque = data >= maxGL/(nbr_couche+5) * i;
    contourf(masque)
    data_msq(:,:,i) = masque;
    img = getframe(gcf);
    imwrite(img.cdata,strcat(rep_echtlln,'/', new_name,'_masque',int2str(i),'.png'))
    i = i + 1;
end
analyse_masque(new_name,data, data_msq,nbr_couche,new_tab,rep_echtlln);
end


%function r = Img_masque
%r = 'Img_masque/';
%end

%function r = Svgd_union
%r = 'Svgd_union';
%end

%function r = Svgd_dnn
%r = 'Svgd_dnn/';
%end
%%%%%%%%%%%%%%%%%%%%%







%FONTION PERMETTANT D'AJOUTER UN ECHANTILLION A LA BIBLIOTQUE
%PREND UN NOM DE FICHIER ET UN NOMBRE DE COUCHE DE MASQUE REQUIS
%SAUVEGARDE LES RESULTATS DANS LES DOSSIERS DEFINI DANS UNE MACRO



%FONTION PERMETTANT D'AJOUTER LES ECHANTILLIONS D'UN DOSSIER A LA BIBLIOTQUE
%PREND LE NOM D'UN DOSSIER ET LE NBR DE COUCHE DE MASQUE
%EXECUTE LA FONCTION AJOUT ECHANTILLION




%ANALYSE MELANGE ECHANTILLION




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

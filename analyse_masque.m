function ret = analyse_masque(name,data,data_masq,nbr_couche,data1,rep_echtll)

x = size(data,1);
y = size(data,2);

srfc = analyse_surface(data_masq,nbr_couche,x,y);
%disp(srfc(2))
smm = ITot(data,data_masq,nbr_couche,x,y);

moy = IMoy(data,data_masq,nbr_couche,x,y);

prmtr = analyse_perimetre(data_masq,nbr_couche,x,y);
%disp (prmtr);
idc_crcl = indice_circula(prmtr,srfc,nbr_couche);

cg = Centre_pics(data,data_masq,nbr_couche,x,y);

ecrt_tp = Sig(data,data_masq,nbr_couche,x,y);

pstn = analyse_position(data_masq,nbr_couche,x,y);
%disp (pstn);
nbr_obj = cpt_objet(data_masq,nbr_couche,x,y);
%disp(nbr_obj)
s_obj = sep_objet(data_masq,nbr_couche);
%disp(s_obj(:,:,1))

cgm = ctr_grv(data_masq,nbr_couche);

ret = struct('data',data,'data_masq',data_masq,'srfc',srfc,'pstn',pstn,'smm',smm,'prmtr',prmtr,'nbr_obj',nbr_obj,'s_obj',s_obj,'moy',moy,'idc_crcl',idc_crcl,'ecrt_tp',ecrt_tp,'cg',cg,'nbr_couche',nbr_couche,'data1',data1,'cgm',cgm);

chmn_dnn = strcat('Bibliothèque/Dnn/', name, '.mat');

rep_echtll = strcat(rep_echtll,'/',name);

save(chmn_dnn,'data','data_masq','srfc','pstn','smm','prmtr','nbr_obj','s_obj','moy','idc_crcl','ecrt_tp','cg','nbr_couche','data1','cgm');
dnnto_excel(chmn_dnn,data1,rep_echtll);
dnn_masque_excel(data,data_masq,nbr_couche,chmn_dnn,rep_echtll)

%xlswrite("Test.xlsx");
end



function dnn_masque_excel(data,data_masq,nbr_couche,file_name,rep_echtll)
x = size(data_masq,1);
y = size(data_masq,2);
nbr_obj = cpt_objet(data_masq,nbr_couche,x,y);
name_file = strrep(file_name,'.mat','');
disp(file_name)
name_file = strcat(rep_echtll,'.xlsx');
[~, ~, raw] = xlsread(name_file,'Données');
nbr_fils = 0;
%nbr_obj_max = recup_nbr_obj_max(nbr_obj,nbr_couche);
for i = 1:nbr_couche
    data_label = bwlabel(data_masq(:,:,i));
    %disp('Objet num :')
    %disp (i);
    if nbr_obj(i) > 1
        for j = 1:nbr_obj(i)

            data_obj = recup_obj(data_label,j,x,y);
            if (veref_obj(data_obj) == 1)
                raw(nbr_couche + 4 + nbr_fils,1) = cellstr(strcat('Obj : ',int2str(i),' - ',int2str(j)));

                srfc = analyse_surface(data_obj,1,x,y);

                raw(nbr_couche + 4 + nbr_fils,3) = num2cell(srfc);

                pstn = analyse_position(data_obj,1,x,y);

                raw(nbr_couche + 4 + nbr_fils,4) = num2cell(pstn(1));

                raw(nbr_couche + 4 + nbr_fils,5) = num2cell(pstn(2));

                raw(nbr_couche + 4 + nbr_fils,6) = num2cell(pstn(3));

                raw(nbr_couche + 4 + nbr_fils,7) = num2cell(pstn(4));

                raw(nbr_couche + 4 + nbr_fils,8) = num2cell(ITot(data,data_obj,1,x,y));

                prmtr = analyse_perimetre(data_obj,1,x,y);

                raw(nbr_couche + 4 + nbr_fils,9) = num2cell(prmtr);

                raw(nbr_couche + 4 + nbr_fils,11) = num2cell(IMoy(data,data_obj,1,x,y));

                raw(nbr_couche + 4 + nbr_fils,12) = num2cell(indice_circula(prmtr,srfc,1));

                raw(nbr_couche + 4 + nbr_fils,13) = num2cell(Sig(data,data_obj,1,x,y));

                cg = Centre_pics(data,data_obj,1,x,y);

                raw(nbr_couche + 4 + nbr_fils,14) = num2cell(cg(1));

                raw(nbr_couche + 4 + nbr_fils,15) = num2cell(cg(2));

                cgm = ctr_grv(data_obj,1);

                raw(nbr_couche + 4 + nbr_fils,16) = num2cell(cgm(1));

                raw(nbr_couche + 4 + nbr_fils,17) = num2cell(cgm(2));

                nbr_fils = nbr_fils + 1;
            end
        end
    end
end
name_file = strcat(rep_echtll,".xlsx");
xlswrite(name_file ,raw, 'Données')
end

function ret = veref_obj(data)
x = size(data,1);
y = size(data,2);
ret = 0;
for i = 1:x
    for j = 1:y
        if (data(i,j) == 1)
            ret = 1;
        end
    end
end
end

%function ret = recup_nbr_obj_max(nbr_obj,nbr_couche)
%ret = 0;
%for i = 1:nbr_couche
%    if (nbr_obj(i) > ret)
%        ret = nbr_obj(i);
%    end
%end
%end



function ret = recup_obj(data,nbr,x,y)
ret = data;
for i = 1:x
    for j = 1:y
        if (data(i,j) == nbr)
            ret(i,j) = 1;
        else
            ret(i,j) = 0;
        end
    end
end
end



function ret = ctr_grv(data,nbr_couche)
%obj_max = recup_nbr_obj_max(x.nbr_obj,x.nbr_couche);
%disp (obj_max)
ret = zeros(nbr_couche,2);
for i = 1:nbr_couche
    %disp(i)
    stats = regionprops(data(:,:,i));
    %disp(i)
    %disp(data(:,:,i))
    %disp(stats)
    %ret(i,1) = round(stats.Centroid(1));
    %ret(i,2) = round(stats.Centroid(2));
    %contourf(nuage_cg)
end
end



%PREND UNE LISTE DE COMPOSANT ISOLE ET 
%FAIT LA FUSION DES MASQUES DES COMPOSANTS
%EN LES SAUVEGARDANTS
%function ret = union_tableau(liste,name_file,nbr_couche)
%ret = zeros(60,591,nbr_couche);
%str = strcat('Svgd_union/' ,'/', name_file);
%
%if (exist(str, "dir") == 0)
%    mkdir (str)
%end
%
%for i = 1:nbr_couche
%    for j = 1:numel(liste)
%        str = strcat('svgd_dnn/', liste(j));
%        disp (str)
%        data = load(str);
%        ret = union_masque(ret,data.data_masq,nbr_couche);
%        contourf(ret(:,:,i))
%        colormap('gray')
%        img = getframe(gcf);
%        imwrite(img.cdata, strcat('Svgd_union/' ,'/', name_file , '/' , name_file , '_masque' , int2str(i) , '.png'))
%    end
%end
%
%end


%FAIT    DATA1 U DATA2 
%function ret = union_masque(data1,data2,nbr_couche)
%ret = zeros(60,591,nbr_couche);
%
%for i = 1:nbr_couche
%    for j = 1:60
%        for k = 1:591
%            if (data1(j,k,i) == 1 || data2(j,k,i) == 1)
%                ret(j,k,i) = 1;
%            end
%        end
%    end
%end
%
%end



function ret = analyse_surface(data,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data(j,k,i) == 1)
                ret(i) = ret(i) + 1;
            end
        end
    end
end

end



%function ret = recup_objet(data,obj,index)
%ret = zeros(60,591);
%
%for i = 1:60
%    for j = 1:591
%        if (data(i,j,index) == obj)
%            ret(60,591) = 1;
%        end
%    end
%end
%end


%PREND DES MATRICES ET TROUVE LES COORDONNEES H
function ret = analyse_position(data,nbr_couche,x,y)
ret = zeros(nbr_couche,4);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data(j,k,i) == 1 && (ret(i,1) == 0 || ret(i,1) > k))
                ret(i,1) = k;
            end
            if (data(j,k,i) == 1 && (ret(i,2) == 0 || ret(i,2) > j))
                ret(i,2) = j;
            end
            if (data(j,k,i) == 1 && (ret(i,3) == 0 || ret(i,3) < k))
                ret(i,3) = k;
            end
            if (data(j,k,i) == 1 && (ret(i,4) == 0 || ret(i,4) < j))
                ret(i,4) = j;
            end
        end
    end
end

end



function ret = analyse_perimetre(data_masq,nbr_couche,x,y)
BW = zeros(x,y,nbr_couche);

for i = 1:nbr_couche
    BW(:,:,i) = bwperim(data_masq(:,:,i));
end

ret = analyse_surface(BW,nbr_couche,x,y);
end



function ret = supp_objet(data,i,j)

if data(i,j) == 1
    data(i,j) = 0;
    if i > 1
        data = supp_objet(data,i - 1, j);
    end
    if i < size(data,1)
        data = supp_objet(data,i + 1, j);
    end
    if j > 1 && data(i,j - 1) == 1
        data = supp_objet(data,i, j - 1);
    end
    if j < size(data,2) && data(i,j + 1) == 1
        data = supp_objet(data,i, j + 1);
    end
end

ret = data;
end



function ret = cpt_objet(data,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if data(j,k,i) == 1
                ret(i) = ret(i) + 1;
                data(:,:,i) = supp_objet(data(:,:,i),j,k);
            end
        end
    end
end

end



function ret = sep_objet(data,nbr_couche)
ret = zeros(size(data,1),size(data,2),nbr_couche);

for i = 1:nbr_couche
   ret(:,:,i) = bwlabel(data(:,:,i));
end

contourf(ret(:,:,i))
end



%function ret = analyse_feret(data_masq)
%end



function ret = ITot(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                ret(i) = ret(i) + data(j,k);
            end
        end
    end
end

end



function ret = IMoy(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);

for i = 1:nbr_couche
    cpt = 0;
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                ret(i) = ret(i) + data(j,k);
                cpt = cpt + 1;
            end
        end
    end
    ret(i) = ret(i)/cpt ;
end

end



%function ret = Analyse_pics(data,data_masq,nbr_couche)
%ret = zeros(nbr_couche,1);
%[max_pic,nbr_pic] = obtenir_pics(data,data_masq);
%end



function [ret, count] = obtenir_pics(data,data_masq)
ret = 0;
count = 0;
for j = 1:60
    for k = 1:591
        if (data_masq(j,k,10) > ret)
                ret = data(j,k,10);
                count = 0;
        end
        if (data_masq(j,k,10) == ret)
            count = count + 1;
        end
    end
end
end



function ret = Recup_data_masq(data,data_masq,nbr_couche)
ret = zeros(60,591,nbr_couche);

for i = 1:nbr_couche
    for j = 1:60
        for k = 1:591
            if (data_masq(j,k,i) == 1)
                ret(j,k,i) = data(j,k,i);
            end
        end
    end
end

end



%function ret = Centre_grav_masque(data, nbr_couche)
%ret = zeros(nbr_couche,2);
%for i = 1:nbr_couche
%    stats = regionprops('table',data(i),'centroid');
%    ret(i,1) = stats.Centroid(1,1); 
%    ret(i,2) = stats.Centroid(1,2); 
%end
%end

function ret = Sig(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,1);
moyenne = IMoy(data,data_masq,nbr_couche,x,y);

for i = 1:nbr_couche
    cpt = 0;
    for j = 1:x
        for k = 1:y
            if (data_masq(j,k,i) == 1)
                tmp = data(j,k) - moyenne(i);
                if (tmp < 0)
                    tmp = tmp * (-1);
                end
                ret(i) = ret(i) + tmp * tmp ;
                cpt = cpt + 1;
            end
        end
    end
    ret(i) = ret(i)/cpt ;
    ret(i) = sqrt(ret(i));
end

end


function ret = Centre_pics(data,data_masq,nbr_couche,x,y)
ret = zeros(nbr_couche,2);
for i = 1:nbr_couche
    for j = 1:x
        for k = 1:y
            if ( data_masq(j,k,i) == 1)
                if ret(i,1) == 0 && ret(i,2) == 0
                    ret(i,1) = j;
                    ret(i,2) = k;
                end
                if data(j,k) > data(ret(i,1),ret(i,2))
                    ret(i,1) = j;
                    ret(i,2) = k;
                end
            end
        end
    end
end

end

function ret = indice_circula(perimetre,air,index)
ret = zeros(index,1);

for i = 1:index
    ret(i) = (perimetre(i) * perimetre(i)) / (4 * pi *air(i));
end

end